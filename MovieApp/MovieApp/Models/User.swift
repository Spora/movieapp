// 
//  User.swift
//  MovieApp
//
//  Created by Ville Raitio on 13.2.2022.
//  
//
	

import Foundation
import Combine

public class User:ObservableObject {
    
    //MARK: Observables
    
    /// Called whenever a genre has been modified
    var genreModified = PassthroughSubject<Genre,Never>()
    /// Called when the set of genres has changed
    var didChange = PassthroughSubject<Set<Genre>,Never>()
    
    
    //MARK: Constants and variables
    
    /// String key used for users genrepreferences stored in standard UserDefaults.
    private let genrePrefsKey:String = "genrePreferences"
    /// Users genre preferences
    var genrePreferences:Set<Genre> {
        didSet {
            UserDefaults.standard.setValue(genrePreferences.compactMap{String($0.rawValue)}, forKey: genrePrefsKey)
            didChange.send(genrePreferences)
        }
    }
    
    //MARK: Init
    
    init(){
        if let userDefaults = UserDefaults.standard.stringArray(forKey: genrePrefsKey) {
            self.genrePreferences = Set(userDefaults.compactMap{Genre.init(rawValue: $0)})
        }else{
            self.genrePreferences = []
        }
    }
    
    init(genrePreferences:Set<Genre>){
        self.genrePreferences = genrePreferences
    }
    
    //MARK: Functions
    
    func togglePreference(_ genre:Genre){
        if self.genrePreferences.contains(genre){
            self.genrePreferences.remove(genre)
            genreModified.send(genre)
        }else{
            self.genrePreferences.insert(genre)
            genreModified.send(genre)
        }
    }
}
