// 
//  MovieList.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//
	

/// This basic Model contains a list title (eg. Genre, Suggestions) and a list of movies.
public struct MovieList {
    /// Title for the list (eg. name of the genre, "Recommendations" or "Weekly Discovery" ala spotify)
    public let listTitle:String
    /// List of movies that the list will display
    public let movies:[Movie]
}

public extension MovieList {
    
    /// Populates an array of MovieLists based on genre preferences
    /// - Parameters:
    ///   - movies: Full list of movies (we'll assume this is what we get from the API)
    ///   - genrePreferences: List of preferred genres
    /// - Returns: A list for Recommendations (see generateRecommendationsFromMovies) and a list for each genre
    static func populateUserPreferredGenreListData(movies:[Movie], genrePreferences:Set<Genre>) -> [MovieList] {
        //score all movies
        let scoredMovies = Movie.scoreMovieArray(movies: movies, genrePreferences: genrePreferences)
        //let's start by sorting the whole thing
        let sortedMovies = scoredMovies.sorted(by: {($0.genrePrefScore ?? 0, $0.userRating ?? 0) > ($1.genrePrefScore ?? 0, $1.userRating ?? 0)})
        var preferredGenresData:[MovieList] = []
        preferredGenresData.append(generateRecommendationsFromMovies(movies: sortedMovies))
        let unbiasedGenres:Set<Genre> = Genre.getRandomGenres(count: 6, preselectedGenres: genrePreferences)
        preferredGenresData.append(contentsOf: generateGenreListFromMovies(movies: sortedMovies, genres: unbiasedGenres))
        return preferredGenresData
    }
    
    /// Splices an array of size 10 from the given array of movies
    /// - Parameters:
    ///   - movies: List of movies (sorted before this function)
    /// - Returns: Movie array
    static func generateRecommendationsFromMovies(movies:[Movie]) -> MovieList{
        //let's return the top 10 (provided that the input array size is at least 10)
        let listCap = movies.count >= 10 ? 10 : movies.count
        return .init(listTitle: "Recommendations", movies: Array(movies[0 ..< listCap]))
    }
    
    /// Generates a list of MovieList based on genres input. Each lists title will be the raw value of the genre enum
    /// - Parameters:
    ///   - movies: List of movies
    ///   - genres: Genres to filter the list
    /// - Returns: Array of MovieList objects
    static func generateGenreListFromMovies(movies:[Movie], genres:Set<Genre>) -> [MovieList] {
        var listOfMovieListsByGenre:[MovieList] = []
        for genre in genres {
            let moviesForGenre = movies.filter({$0.genres.contains(genre)})
            
            listOfMovieListsByGenre.append(.init(listTitle: genre.rawValue, movies: moviesForGenre))
            
        }
        return listOfMovieListsByGenre
    }
}
