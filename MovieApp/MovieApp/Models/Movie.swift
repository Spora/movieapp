// 
//  Movie.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//

import Foundation


/// Genre enumerable. Note that it could also be an option to fetch the genres from the API
public enum Genre:String, Codable, CaseIterable {
    case Action
    case Adventure
    case Animated
    case Biography
    case Comedy
    case Crime
    case Documentary
    case Drama
    case Family
    case Fantasy
    case Noir
    case Horror
    case Musical
    case Mystery
    case Romance
    case SciFi = "Science Fiction"
    case Sports
    case Superhero
    case Thriller
    case War
    case Western
}

public extension Genre {
    static func getRandomGenres(count:Int, preselectedGenres:Set<Genre>) -> Set<Genre> {
        var genres:Set<Genre> = []
        var pool:[Genre] = Genre.allCases
        for preselectedGenre in preselectedGenres {
            if let indexToRemove = pool.firstIndex(of: preselectedGenre) {
                pool.remove(at: indexToRemove)
            }
        }
        
        genres = preselectedGenres
        
        if genres.count > count {
            //instead of adding genres, we will remove genres until we have the correct amount
            repeat {
                genres.remove(genres.randomElement()!)
            } while genres.count > count
            
            return genres
        }
        for _ in 0 ..< count-preselectedGenres.count {
            let selection = Int.random(in: 0...pool.count-1)
            //adds to random genres
            genres.insert(pool[selection])
            //removes the random index from the genre pool
            pool.remove(at: selection)
        }
        return genres
    }
}

/// Movie structure
public struct Movie:Codable {
    /// Name of the movie
    let title:String
    //TODO: all parameters declared as let are variables just because the sample json we have is missing them (so we will init them post init)
    /// Subtitle (note not calling this as subtitle because it would be confusing if movie subtitles are used)
    var headline:String? = ""
    /// Year of release
    let year:Int
    /// Longer text to display in the movie's page (could be named summary as well)
    var description:String? = ""
    /// User rating from 0.0 to 10.0
    var userRating:Double? = 0
    /// URL for poster image
    var posterImgUrl:String? = ""
    /// URL for trailer movie
    var trailerUrl:String? = ""
    /// List of genres
    let genres:[Genre]
    /// Adjusted score used as an additional sorting layer over userRating
    var genrePrefScore:Int? = 0
    let cast:[String]?
    /*
     TODO: other parameters to consider:
     - languages (spoken/subtitled)
     - subtitle url for selected language?
     - age rating
     - directors, producers, actors, etc...
     */
    
    
    public mutating func generateUserRating(){
        self.userRating = Double.random(in: 6.5 ... 10.0)
    }
}

extension Movie {
    
    public var castString:String? {
        return cast?.joined(separator: ",")
    }
    
    /// Sets the genrePrefScore fo the movie based on userRating and the input genrePreferences
    /// - Parameter genrePreferences: Array of Genre to adjust scoring based on preferences
    public mutating func setUserPrefScore(genrePreferences:Set<Genre>){
        self.genrePrefScore = 0
        for genrePreference in genrePreferences {
            if(self.genres.contains(genrePreference)){
                self.genrePrefScore! += 1
            }
        }
        //to avoid recommending movies with bad ratings, we could adjust the score a bit based on rating
        //if our self generated score is missing, let's add it here...
        if self.userRating == nil {
            self.generateUserRating()
        }
        
        let highRatingThreshold = 9.0
        let lowRatingThreshold = 7.5
        if self.userRating! >= highRatingThreshold {
            self.genrePrefScore! += 1
        }else if self.userRating! <= lowRatingThreshold {
            self.genrePrefScore! -= 2
        }
    }
    
    /// Creates a userpreference score based on genre preferences and the user rating of each movie.
    /// - Parameters:
    ///   - movies: List of movies to score
    ///   - genrePreferences: Preferences for scoring the movie
    /// - Returns: Clone of movies Array with genrePrefScore set for each entry
    public static func scoreMovieArray(movies:[Movie],genrePreferences:Set<Genre>) -> [Movie]{
        var scoredMovies:[Movie] = []
        for movie in movies {
            var scoredMovie = movie
            scoredMovie.setUserPrefScore(genrePreferences: genrePreferences)
            scoredMovies.append(scoredMovie)
        }
        return scoredMovies
    }
    
    
    /// Returns the rounded userRating
    public var roundedRating:Double{
        guard let userRating = self.userRating else {return 0.0}
        return Double(Double(round(userRating*100.0))/100.0)
    }
    
    public static func loadMoviesFromLocalJSON() -> [Movie]? {
        do {
            if let bundlePath = Bundle.main.path(forResource: "movies",
                                                 ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                
                let decodedData = try decoder.decode([Movie].self,
                                                                   from: jsonData)
                return decodedData
            }
        } catch {
            print(error)
        }
        return nil
    }
    
}


//TODO: This dummydata can be removed when we have an established API
//MARK: Movie dummydata

public class DummyMovieData {
    //some of the more recent trailers that were found from youtube
    private static let trailerUrls:[String] = [
        "https://www.youtube.com/watch?v=BwZs3H_UN3k",
        "https://www.youtube.com/watch?v=fb5ELWi-ekk",
        "https://www.youtube.com/watch?v=aOIHRlWaufY",
        "https://www.youtube.com/watch?v=DHREzAdyCPs",
        "https://www.youtube.com/watch?v=X9ebeNKkc08",
        "https://www.youtube.com/watch?v=FPxheU-E0Tg",
        "https://www.youtube.com/watch?v=HKJ4Thgk1Js",
        "https://www.youtube.com/watch?v=EWXb-Twcbi4",
        "https://www.youtube.com/watch?v=oMSdFM12hOw"
    ]
    //these are small thumbnails found from IMDB
    public static let posterImages:[String] = [
        "https://m.media-amazon.com/images/M/MV5BMzhhNWVmNzItOTFlOC00OTAwLWFmNjgtMDBhOWM2YTkwZDcwXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BZjllZjE1MWEtYTJhZC00MWIyLTliMjEtYzM3ODc4YzQ2MjFlXkEyXkFqcGdeQXVyODIyOTEyMzY@._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BOTY1ZGM2YzQtMTBjZC00NjE2LWJlNzUtYjA0YjYxNzBjMmRjXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BMmYyMTE3ZDItOGQwZC00ODgwLTkzN2YtOTIxNTI1Yjg0ZjE0XkEyXkFqcGdeQXVyMTM1MTE1NDMx._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BOTY1ZGM2YzQtMTBjZC00NjE2LWJlNzUtYjA0YjYxNzBjMmRjXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BNzQ4MGYzYzAtNjJlOC00NTdkLTlmNTAtMmZjMzZmZTg2YTEzXkEyXkFqcGdeQXVyMjkwOTAyMDU@._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BMmYyMTE3ZDItOGQwZC00ODgwLTkzN2YtOTIxNTI1Yjg0ZjE0XkEyXkFqcGdeQXVyMTM1MTE1NDMx._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BNmZhMTFkN2MtMTg0OS00NGQ4LWI5ZmYtMjZhYzVhMWE1ZjRkXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BYTExZTdhY2ItNGQ1YS00NjJlLWIxMjYtZTI1MzNlMzY0OTk4XkEyXkFqcGdeQXVyMTEyMjM2NDc2._V1_UX67_CR0,0,67,98_AL_.jpg",
        "https://m.media-amazon.com/images/M/MV5BMWRiZGQ1NDMtODQ2OS00MDlhLWJkZGYtM2ZmNjlhZThjOWRmXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX67_CR0,0,67,98_AL_.jpg"
    ]
    /// Default lorem text for the longer description of the movie
    public static let lorem:String = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    /// Default lorem headline for dummydata
    public static let loremHeadline:String = "Lorem ipsum dolor sit amet"
    
    /// Default amount of movies that will be generated
    private static let movieCount = 50
    
    private static let minMovieCountPerGenre = 10
    
    /// Sorts the movie based on preference matching and user ratings
    /// - Parameter genres: Preferred genres
    /// - Returns: Sorted movies
    public static func getMoviesSortedByPreference(_ genres:Set<Genre>) -> [Movie]{
        if genres.count == 0 {
            return []
        }
        var movies:[Movie] = []
        for genre in Genre.allCases {
            for _ in 0 ..< minMovieCountPerGenre {
                var movie = generateMovieWithGenre(genre)
                movie.setUserPrefScore(genrePreferences: genres)
                movies.append(movie)
            }
        }
        return movies.sorted(by: {($0.genrePrefScore ?? 0, $0.userRating ?? 0) > ($1.genrePrefScore ?? 0, $1.userRating ?? 0)})
    }
    
    /// Creates an array of movies for preview purposes
    /// - Parameter genres: list of genres
    /// - Returns: Array of genres
    public static func dummyMovieArrayForPreview(_ genres:Set<Genre>)->[Movie]{
        var movies:[Movie] = []
        for i in 0...5 {
            movies.append(dummyMovieForPreview(title: "Demo Movie \(i)",genres:genres))
        }
        return movies
    }
    
    /// Creates a movie object for Preview purposes
    /// - Parameters:
    ///   - title: Movie title
    ///   - headline: Movie headline
    ///   - released: Year released
    ///   - description: Longer description for movie
    ///   - userRating: User rating from movie database
    ///   - posterImgUrl: URL string for image
    ///   - trailerUrl: URL string for trailer video
    ///   - genres: Movie genres
    /// - Returns: Movie object
    public static func dummyMovieForPreview(
        title:String = "Test Movie",
        headline:String = "This is a test movie",
        released:Int = 2022,
        description:String = lorem,
        userRating:Double = 7.455125821,
        posterImgUrl:String = "https://m.media-amazon.com/images/M/MV5BMzhhNWVmNzItOTFlOC00OTAwLWFmNjgtMDBhOWM2YTkwZDcwXkEyXkFqcGdeQXVyMTEzMTI1Mjk3._V1_UX67_CR0,0,67,98_AL_.jpg",
        trailerUrl:String = "https://www.youtube.com/watch?v=BwZs3H_UN3k",
        genres:Set<Genre> = [.Animated,.SciFi,.Comedy]) -> Movie {
        return .init(
            title: title, headline: headline, year: released, description: description, userRating: userRating, posterImgUrl: posterImgUrl, trailerUrl: trailerUrl, genres: Array(genres), cast: nil)
    }
    
    /// Generates dummy data for recommendation list (will have movies from mixed genres)
    /// - Parameter genres: List of genres fetched
    /// - Returns: List of movies that each has at least one of the genres in the input parameter
    private static func generateMovieWithGenres(_ genres:Set<Genre>) -> Movie {
        //omit some genres out
        let strippedGenres = strippedGenres(genres)
        return .init(
            title: generateTitle(strippedGenres),
            headline: loremHeadline,
            year: Int.random(in: 1950...2022),
            description: lorem,
            userRating: Double.random(in: 5.0...10.0),
            posterImgUrl: DummyMovieData.posterImages.randomElement() ?? DummyMovieData.posterImages[0],
            trailerUrl: DummyMovieData.trailerUrls.randomElement() ?? DummyMovieData.trailerUrls[0],
            genres: Array(strippedGenres),
            cast: nil
        )
    }
    
    /// Generates a movie with at least the requested genre
    /// - Parameter genre: The movie will have at least this genre
    /// - Returns: Movie object
    private static func generateMovieWithGenre(_ genre:Genre) -> Movie{
        let strippedGenre = strippedGenres([genre])
        return .init(
            title: generateTitle(strippedGenre),
            headline: loremHeadline,
            year: Int.random(in: 1950...2022),
            description: lorem,
            userRating: Double.random(in: 5.0...10.0),
            posterImgUrl: DummyMovieData.posterImages.randomElement() ?? DummyMovieData.posterImages[0],
            trailerUrl: DummyMovieData.trailerUrls.randomElement() ?? DummyMovieData.trailerUrls[0],
            genres: Array(strippedGenre),
            cast: nil)
    }
    
    /// Randomly strip (or don't strip) some genre's from the input list. Will always retain at least one genre.
    /// - Parameter genres: List of possible genres
    /// - Returns: Stripped down list of genres
    private static func strippedGenres(_ genres:Set<Genre>) -> Set<Genre> {
        var strippedGenres = genres
        for member in strippedGenres {
            if strippedGenres.count == 1 {
                //at this stage we have already removed all but one genre
                break
            }
            //let random do it's thing and remove the genre if it pleases
            if Bool.random() {
                strippedGenres.remove(member)
            }
        }
        //odds of adding a genre are higher the first time added.
        var addPossibility = 0.8
        let addPossibilityDecay = 0.3
        
        for genreCandidate in Genre.allCases {
            if Double.random(in: 0...1.0) < addPossibility {
                //we shouldn't add already existing or stripped genres
                if !genres.contains(genreCandidate){
                    strippedGenres.insert(genreCandidate)
                    addPossibility -= addPossibilityDecay
                }
                
            }else{//stop adding once this fails even once
                break
            }
        }
        
        return strippedGenres
    }
    
    /// Generates a very basic title for the movie from the listed genres
    /// - Parameter genres: List of genres
    /// - Returns: Generated title
    private static func generateTitle(_ genres:Set<Genre>) -> String{
        //some of the genres might be omitted out
        var title:String = ""
        var i = 0
        for genre in genres {
            title.append(genre.rawValue)
            //to output reasonable title lengths, only take 2 genres max!
            if i == 1 {
                break
            }
            i += 1
        }
        title.append(" the Movie")
        return title
    }
    
}
