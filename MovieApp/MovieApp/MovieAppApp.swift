// 
//  MovieAppApp.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//
	

import SwiftUI

@main
struct MovieAppApp: App {
    var body: some Scene {
        
        WindowGroup {
            ContentView()
        }
    }
}
