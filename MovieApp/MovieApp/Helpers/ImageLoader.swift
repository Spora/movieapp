// 
//  ImageLoader.swift
//  MovieApp
//
//  Created by Ville Raitio on 13.2.2022.
//
	
import Combine
import Foundation

/// Solution for not using AsyncImage (iOS 15 required). This was found on StackOverflow: https://stackoverflow.com/a/60677690
class ImageLoader: ObservableObject {
    var didChange = PassthroughSubject<Data, Never>()
    var data = Data() {
        didSet {
            didChange.send(data)
        }
    }
    
    /// Loads the Image data asyncrhonously.
    /// - Parameter urlString: URL to image in string format
    init(urlString:String) {
        guard let url = URL(string: urlString) else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else { return }
            DispatchQueue.main.async {
                self.data = data
            }
        }
        task.resume()
    }
}
