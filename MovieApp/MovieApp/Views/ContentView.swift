// 
//  ContentView.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//
	

import SwiftUI

struct ContentView: View {
    @ObservedObject var user:User = .init()
    var allMovies:[Movie]? = Movie.loadMoviesFromLocalJSON()
    @State var preferencesChanged = false
    
    @State var movieListsWithState:[MovieList] = []
    
    var body: some View {
        
        NavigationView{
            ScrollView(.vertical, showsIndicators: true){
                VStack{
                    Spacer()
                    ForEach($movieListsWithState, id: \.listTitle){ list in
                        MovieCardList(movieListModel: list.wrappedValue)
                    }
                }
                .padding(.top)
                .navigationTitle("Home")
                .navigationBarTitleDisplayMode(.inline)
                .toolbar{
                    NavigationLink(destination: UserPageView(user: user)){
                        Image(systemName: "person.fill")
                    }
                }
            }.onAppear{
                if preferencesChanged || movieListsWithState.isEmpty {
                    //refetch movies list...
                    //let movies = //DummyMovieData.getMoviesSortedByPreference(user.genrePreferences)
                    if let movies = allMovies {
                        $movieListsWithState.wrappedValue = MovieList.populateUserPreferredGenreListData(movies: movies, genrePreferences: user.genrePreferences)
                    }
                }
                preferencesChanged = false
            }.onReceive(user.didChange) { _ in
                self.preferencesChanged = true
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(
            allMovies: DummyMovieData.dummyMovieArrayForPreview([.Animated,.Horror,.Comedy,.SciFi])
        )
    }
}

