// 
//  MovieCardView.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//
	

import SwiftUI

struct MovieCardView:View {
    var movie:Movie
    @ObservedObject var imageLoader:ImageLoader// = ImageLoader(urlString: movie.posterImgUrl)
    @State var image:UIImage = UIImage()
    
    init(movie:Movie){
        self.movie = movie
        
        self.imageLoader = .init(urlString: movie.posterImgUrl ?? "")//TODO: handle image missing in a smarter way
        
    }
    
    var body: some View{
        NavigationLink(destination: MoviePageView(movie: movie, image: image)){
            Image(uiImage: image)
            .resizable()
            .aspectRatio(contentMode: .fill)
            .frame(width: 240, height: 300)
            .clipped()
            .onReceive(imageLoader.didChange){ data in
                self.image = UIImage(data: data) ?? UIImage()
            }
            .background(Color.black)
            .overlay(
                VStack{
                    Text(movie.title)
                        .font(.headline)
                        .foregroundColor(.white)
                        .padding(.top)
                    Text(movie.headline ?? DummyMovieData.loremHeadline)
                        .font(.subheadline)
                        .foregroundColor(.white)
                    HStack{
                        Text(String(movie.year))
                            .font(.footnote)
                            .foregroundColor(.white)
                        Spacer()
                        Label(
                            title: {
                                Text(String(movie.roundedRating))
                                    .foregroundColor(.white)
                            }, icon: {
                                Image(systemName: "star.fill")
                                    .foregroundColor(.yellow)
                            }
                        )
                        .font(.footnote)
                    }
                    .padding()
                }
                    .background(Color.black.opacity(0.7))
                ,
                alignment: .bottom
            )
            
            .cornerRadius(25)
        }
    }
}
struct MovieCardView_Previews: PreviewProvider {
    static var previews: some View {
        MovieCardView(movie: DummyMovieData.dummyMovieForPreview())
    }
}
