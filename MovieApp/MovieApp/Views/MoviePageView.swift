// 
//  MoviePageView.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//
	

import SwiftUI

struct MoviePageView:View {
    
    let movie:Movie
    let image:UIImage
    
    var body: some View{
        ScrollView(.vertical){
            VStack(alignment: .leading, spacing: 8){
                Image(uiImage: image)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(height: 340, alignment: .top)
                    .background(Color.black)
                    .clipped()
                VStack{
                    Text(movie.title)
                        .font(.title)
                    Text(movie.headline ?? DummyMovieData.loremHeadline)
                        .font(.headline)
                    Spacer(minLength: 16).fixedSize()
                    Text(movie.description ?? DummyMovieData.lorem)
                        .font(.body)
                        .lineLimit(10)
                    
                    if (movie.castString != nil){
                        Spacer(minLength: 16).fixedSize()
                        Text("Cast:").font(.headline)
                        Spacer(minLength: 8).fixedSize()
                        Text(movie.castString ?? "").font(.body).lineLimit(10)
                    }
                }.padding()
                Spacer()
                HStack(alignment: .center){
                    Spacer()
                    Text("Released:")
                    Text(String(movie.year))
                    Spacer()
                    Label(
                        title: {
                            Text(String(movie.roundedRating))
                                .foregroundColor(.white)
                        }, icon: {
                            Image(systemName: "star.fill")
                                .foregroundColor(.yellow)
                        }
                    )
                    Spacer()
                }
                .padding()
                Spacer()
                ScrollView(.horizontal, showsIndicators: true){
                    HStack(alignment: .center){
                        Spacer()
                        ForEach(movie.genres, id: \.rawValue){ genre in
                            Text(genre.rawValue)
                                .padding(8)
                                .background(Color.gray.opacity(0.4))
                                .cornerRadius(8)
                                
                        }
                            
                        Spacer()
                    }
                }
                Spacer()
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        //.edgesIgnoringSafeArea([.top])
    }
}

struct MoviePageView_Previews: PreviewProvider {
    static var previews: some View {
        MoviePageView(movie: DummyMovieData.dummyMovieForPreview(title:"Test Movie"),image: UIImage(named: "poster_placeholder") ?? UIImage())
    }
    
}
