// 
//  UserPageView.swift
//  MovieApp
//
//  Created by Ville Raitio on 13.2.2022.
//  
//
	

import SwiftUI

struct UserPageView:View {
    
    @ObservedObject var user:User
    var columns: [GridItem] =
             Array(repeating: .init(.flexible()), count: 3)
    
    init(user:User){
        self.user = user
    }
    
    var body: some View{
        VStack{
            Text("Select your genre preferences:")
                .font(.title)
            LazyVGrid(columns: columns){
                
                ForEach(Genre.allCases,id:\.rawValue){ genre in
                    GenreElement(user: user, genre: genre)
                }
            }
        }
    }
}

struct UserPageView_Previews: PreviewProvider {
    static var previews: some View {
        UserPageView(user: .init(genrePreferences: [.Animated,.Crime,.Romance]))
    }
    
}
