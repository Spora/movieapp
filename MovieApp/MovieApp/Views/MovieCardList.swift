// 
//  MovieCardList.swift
//  MovieApp
//
//  Created by Ville Raitio on 12.2.2022.
//  
//
	
import SwiftUI

struct MovieCardList:View {
    var movieListModel:MovieList
    
    var body: some View{
        
        VStack{
            Text(movieListModel.listTitle)
                .font(.largeTitle)
            
            ScrollView(.horizontal, showsIndicators: false){
                
                HStack(spacing:20){
                    Spacer()
                    
                    ForEach(movieListModel.movies, id: \.title){ movie in
                        
                            MovieCardView(movie: movie)
                        
                    }
                    
                    Spacer()
                }
        
            }
        }
    }
}

struct MovieCardList_Previews: PreviewProvider {
    static var previews: some View {
        MovieCardList(movieListModel: .init(
                listTitle: Genre.Animated.rawValue,
                movies:
                    DummyMovieData.dummyMovieArrayForPreview([.Animated])
            )
        )
    }
}

