// 
//  GenreElement.swift
//  MovieApp
//
//  Created by Ville Raitio on 13.2.2022.
//  
//
	
import SwiftUI


struct GenreElement:View {
    
    @ObservedObject var user:User
    @State private var genreSelected:Bool
    var genre:Genre
    
    init(user:User, genre:Genre){
        self.user = user
        self.genre = genre
        genreSelected = user.genrePreferences.contains(genre)
    }
    
    var body: some View {
        Text(genre.rawValue)
            .padding(8)
            .background(
                self.$genreSelected.wrappedValue ? Color.green.opacity(0.4) : Color.gray.opacity(0.4)
            )
            .cornerRadius(8)
            .onTapGesture {
                user.togglePreference(genre)
            }
            .onReceive(user.genreModified){ modifiedGenre in
                if modifiedGenre == genre {
                    self.$genreSelected.wrappedValue = user.genrePreferences.contains(modifiedGenre)
                }
            }
    }
}
