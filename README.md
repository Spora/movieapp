# Movie app

A very basic foundation for a movies listing app. No API is currently used, movies are read from a local JSON and then based on user's genre settings (and user ratings) the movies are sorted.

Languages and frameworks used:

SwiftUI, MVVM architecture

## Sorting logic

Currently the sorting logic is handled so that each movie is scored based on how many genre's overlap the user's genre preferences. Higher rating movies will get a higher priority.

Note that currently the userRating is always generated during launch (bogus data!). In the final app the API would provide these parameters (as well as host any of the images, currently they are picked from wikipedia so this is by no means for production use!)

**Models/MoviesetUserPrefScore**

> adjust this function to change how sorting works (eg. maybe we want to weigh the genre specs more than the userRating)
